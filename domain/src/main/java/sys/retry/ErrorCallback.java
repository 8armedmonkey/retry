package sys.retry;

public interface ErrorCallback {

    void onError(RetryException error);

}
