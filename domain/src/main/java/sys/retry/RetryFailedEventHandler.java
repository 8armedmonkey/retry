package sys.retry;

import sys.event.EventHandler;

public interface RetryFailedEventHandler extends EventHandler<RetryFailedEvent> {
}
