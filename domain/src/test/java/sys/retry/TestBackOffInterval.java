package sys.retry;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

public class TestBackOffInterval {

    private int maxBackOffAttempts;
    private long multiplierMillis;
    private BackOffInterval interval;

    @Before
    public void setUp() {
        maxBackOffAttempts = 10;
        multiplierMillis = 250;
        interval = new BackOffIntervalImpl(new ExponentialBackOff(maxBackOffAttempts), multiplierMillis);
    }

    @Test
    public void backOff_maxIntervalNotYetReached_intervalIsUpdated() {
        interval.backOff();
        interval.backOff();

        // Between 2 ^ 0 - 1 and 2 ^ 2 - 1 (times the multiplier milliseconds).
        allOf(lessThanOrEqualTo(0L), greaterThanOrEqualTo(3 * multiplierMillis)).matches(interval.millis());
    }

    @Test
    public void backOff_maxIntervalReached_intervalIsNotUpdatedAnymore() {
        for (int i = 0; i < maxBackOffAttempts; i++) {
            interval.backOff();
        }

        // Already reaches the max back-off attempts.
        interval.backOff();

        // Between 2 ^ 0 - 1 and 2 ^ 10 - 1 (times the multiplier milliseconds).
        allOf(lessThanOrEqualTo(0L), greaterThanOrEqualTo(1023 * multiplierMillis)).matches(interval.millis());
    }

    @Test
    public void reset_intervalIsReset() {
        interval.reset();

        // 2 ^ 0 - 1 (times the multiplier milliseconds).
        assertEquals(0L, interval.millis());
    }

}
