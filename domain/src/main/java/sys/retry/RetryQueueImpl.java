package sys.retry;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class RetryQueueImpl<R extends Retriable> implements RetryQueue<R> {

    private final PriorityQueue<R> queue;
    private final Map<String, R> lookup;

    public RetryQueueImpl(RetriableComparator<R> comparator) {
        queue = new PriorityQueue<>(1, comparator);
        lookup = new HashMap<>();
    }

    @Override
    public void put(R retriable) {
        queue.offer(retriable);
        lookup.put(retriable.key(), retriable);
    }

    @Override
    public R take() {
        R retriable;

        if ((retriable = queue.poll()) != null) {
            lookup.remove(retriable.key());
            return retriable;
        } else {
            throw new EmptyQueueException();
        }
    }

    @Override
    public R remove(String key) {
        R retriable;

        if ((retriable = lookup.remove(key)) != null) {
            queue.remove(retriable);
        }
        return retriable;
    }

    @Override
    public boolean contains(String key) {
        return lookup.containsKey(key);
    }

    @Override
    public boolean isEmpty() {
        return queue.isEmpty();
    }

    @Override
    public void clear() {
        queue.clear();
        lookup.clear();
    }

    @Override
    public long earliestRetryAtTimeMillis() {
        assertNotEmpty();
        return queue.peek().retryAtTimeMillis();
    }

    private void assertNotEmpty() throws EmptyQueueException {
        if (isEmpty()) {
            throw new EmptyQueueException();
        }
    }

}
