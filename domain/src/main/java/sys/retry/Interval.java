package sys.retry;

@SuppressWarnings("WeakerAccess")
public interface Interval {

    /**
     * Get the interval in milliseconds.
     */
    long millis();

}
