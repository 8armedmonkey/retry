package sys.retry;

public class ConstantInterval implements Interval {

    private final long millis;

    public ConstantInterval(long millis) {
        this.millis = millis;
    }

    @Override
    public long millis() {
        return millis;
    }

}
