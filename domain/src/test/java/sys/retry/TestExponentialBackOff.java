package sys.retry;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class TestExponentialBackOff {

    private int max;
    private BackOff backOff;

    @Before
    public void setUp() {
        max = 10;
        backOff = new ExponentialBackOff(max);
    }

    @Test
    public void current_currentCounterIsReturned() {
        backOff.increment();
        backOff.increment();
        backOff.increment();
        backOff.increment();

        // Between 0 and 2 ^ 4 - 1 = 15
        lessThanOrEqualTo(15).matches(backOff.current());
    }

    @Test
    public void increment_hasNotReachedMaxCounter_backOffTimeIncreased() {
        // Between 0 and 2 ^ 1 - 1 = 1
        lessThanOrEqualTo(1).matches(backOff.increment());

        // Between 0 and 2 ^ 2 - 1 = 3
        lessThanOrEqualTo(3).matches(backOff.increment());

        // Between 0 and 2 ^ 3 - 1 = 7
        lessThanOrEqualTo(7).matches(backOff.increment());

        // Between 0 and 2 ^ 4 - 1 = 15
        lessThanOrEqualTo(15).matches(backOff.increment());

        // Between 0 and 2 ^ 5 - 1 = 31
        lessThanOrEqualTo(31).matches(backOff.increment());
    }

    @Test
    public void increment_hasReachedMaxCounter_backOffTimeRemains() {
        for (int i = 0; i < max; i++) {
            backOff.increment();
        }

        // 2 ^ 10 - 1 = 1023
        lessThanOrEqualTo(1023).matches(backOff.increment());
    }

    @Test
    public void reset_counterIsReset() {
        backOff.increment();

        // 2 ^ 0 - 1 = 0
        equalTo(0).matches(backOff.reset());
        equalTo(0).matches(backOff.current());
    }


}
