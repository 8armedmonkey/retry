package sys.retry;

public interface BackOff {

    int increment();

    int current();

    int reset();

}
