package sys.retry;

import sys.event.EventHandler;

public interface RetrySuccessEventHandler extends EventHandler<RetrySuccessEvent> {
}
