package sys.test.provider;

import sys.retry.BackOffInterval;
import sys.retry.BaseBackOffStrategy;

public class SimpleBackOffStrategy extends BaseBackOffStrategy {

    private final FatalErrorChecker fatalErrorChecker;

    public SimpleBackOffStrategy(
            int maxRetries,
            BackOffInterval backOffInterval,
            FatalErrorChecker fatalErrorChecker) {

        super(maxRetries, backOffInterval);
        this.fatalErrorChecker = fatalErrorChecker;
    }

    @Override
    public boolean isFatalError(Throwable failedRetryCause) {
        return fatalErrorChecker.isFatalError(failedRetryCause);
    }

    public interface FatalErrorChecker {

        boolean isFatalError(Throwable failedRetryCause);

    }

}
