package sys.retry;

import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

public class RetrySystemImpl<R extends Retriable> implements RetrySystem<R> {

    private final RetryQueue<R> retryQueue;
    private final SingleTaskScheduler scheduler;
    private final EventBroadcaster eventBroadcaster;
    private long earliestRetryAtTimeMillis;

    @SuppressWarnings("WeakerAccess")
    public RetrySystemImpl(
            RetryQueue<R> retryQueue,
            SingleTaskScheduler scheduler,
            EventBroadcaster eventBroadcaster) {

        this.retryQueue = retryQueue;
        this.scheduler = scheduler;
        this.eventBroadcaster = eventBroadcaster;

        updateEarliestRetryAtTimeMillis();
    }

    @Override
    public void put(R retriable) {
        retryQueue.put(retriable);

        if (earliestRetryAtTimeMillis > retriable.retryAtTimeMillis()) {
            scheduleNextRetry();
        }
    }

    @Override
    public void remove(String key) {
        retryQueue.remove(key);
    }

    @Override
    public boolean contains(String key) {
        return retryQueue.contains(key);
    }

    @Override
    public boolean isQueueEmpty() {
        return retryQueue.isEmpty();
    }

    @Override
    public <E extends Event> EventBroadcaster.Subscription subscribe(
            Class<E> eventClass, EventHandler<E> eventHandler) {

        return eventBroadcaster.subscribe(eventClass, eventHandler);
    }

    @Override
    public void onRetrySuccess(Retriable retriable) {
        eventBroadcaster.broadcast(new RetrySuccessEvent(retriable));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onRetryError(RetryException error) {
        if (error.shouldRetryAgain()) {
            retryQueue.put((R) error.getRetriable());
        }

        eventBroadcaster.broadcast(new RetryFailedEvent(error));
    }

    private void takeInternal() {
        retryQueue.take().retry(this::onRetrySuccess, this::onRetryError);
        scheduleNextRetry();
    }

    private void updateEarliestRetryAtTimeMillis() {
        if (!retryQueue.isEmpty()) {
            earliestRetryAtTimeMillis = retryQueue.earliestRetryAtTimeMillis();
        } else {
            earliestRetryAtTimeMillis = Long.MAX_VALUE;
        }
    }

    private void scheduleNextRetry() {
        updateEarliestRetryAtTimeMillis();

        if (!retryQueue.isEmpty()) {
            scheduler.schedule(this::takeInternal, earliestRetryAtTimeMillis);
        }
    }

}
