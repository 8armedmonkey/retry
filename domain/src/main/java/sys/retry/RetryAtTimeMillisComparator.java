package sys.retry;

/**
 * Comparator for the values of {@link Retriable#retryAtTimeMillis()}.
 */
public class RetryAtTimeMillisComparator implements RetriableComparator<Retriable> {

    @Override
    public int compare(Retriable o1, Retriable o2) {
        return Long.compare(o1.retryAtTimeMillis(), o2.retryAtTimeMillis());
    }

}
