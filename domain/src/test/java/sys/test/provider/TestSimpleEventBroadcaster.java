package sys.test.provider;

import org.junit.Before;
import org.junit.Test;
import sys.event.EventBroadcaster;
import sys.event.EventBroadcaster.Subscription;
import sys.event.EventHandler;
import sys.retry.*;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class TestSimpleEventBroadcaster {

    private EventBroadcaster eventBroadcaster;

    @Before
    public void setUp() {
        eventBroadcaster = EventBroadcasterProvider.simple();
    }

    @Test
    public void broadcastEvent_eventBroadcastedToHandlers() {
        EventHandler<RetrySuccessEvent> successEventHandler1 = mock(RetrySuccessEventHandler.class);
        EventHandler<RetrySuccessEvent> successEventHandler2 = mock(RetrySuccessEventHandler.class);

        EventHandler<RetryFailedEvent> failedEventHandler = mock(RetryFailedEventHandler.class);

        Subscription successEventSubscription1 = eventBroadcaster.subscribe(RetrySuccessEvent.class, successEventHandler1);
        Subscription successEventSubscription2 = eventBroadcaster.subscribe(RetrySuccessEvent.class, successEventHandler2);
        Subscription failedEventSubscription = eventBroadcaster.subscribe(RetryFailedEvent.class, failedEventHandler);

        eventBroadcaster.broadcast(new RetrySuccessEvent(mock(Retriable.class)));
        eventBroadcaster.broadcast(new RetryFailedEvent(new RetryException(new Exception(), mock(Retriable.class), false)));
        eventBroadcaster.broadcast(new RetrySuccessEvent(mock(Retriable.class)));
        eventBroadcaster.broadcast(new RetrySuccessEvent(mock(Retriable.class)));
        eventBroadcaster.broadcast(new RetryFailedEvent(new RetryException(new Exception(), mock(Retriable.class), false)));

        successEventSubscription1.unsubscribe();

        // Only received by successEventHandler2 since successEventHandler1 already unsubscribed.
        eventBroadcaster.broadcast(new RetrySuccessEvent(mock(Retriable.class)));

        successEventSubscription2.unsubscribe();
        failedEventSubscription.unsubscribe();

        // Won't be received by any of the event handlers as all of them already unsubscribed.
        eventBroadcaster.broadcast(new RetrySuccessEvent(mock(Retriable.class)));
        eventBroadcaster.broadcast(new RetryFailedEvent(new RetryException(new Exception(), mock(Retriable.class), false)));

        verify(successEventHandler1, times(3)).handle(any(RetrySuccessEvent.class));
        verify(successEventHandler2, times(4)).handle(any(RetrySuccessEvent.class));
        verify(failedEventHandler, times(2)).handle(any(RetryFailedEvent.class));
    }

}
