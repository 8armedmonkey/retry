package sys.retry;

public interface Retriable extends Comparable<Retriable> {

    String key();

    long retryAtTimeMillis();

    void retry(SuccessCallback onSuccess, ErrorCallback onError);

    void use(RetryStrategy strategy);

}
