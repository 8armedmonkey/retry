package sys.test.provider;

import sys.retry.ErrorCallback;
import sys.retry.Retriable;
import sys.retry.RetryException;
import sys.retry.SuccessCallback;

import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.lang.System.out;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class RetriableProvider {

    public static Retriable simple(String key, long retryAtTimeMillis) {
        Retriable retriable = mock(Retriable.class);
        when(retriable.key()).thenReturn(key);
        when(retriable.retryAtTimeMillis()).thenReturn(retryAtTimeMillis);

        return retriable;
    }

    public static void success(Retriable retriable) {
        doAnswer(invocation -> {

            out.println(format("[%d] Retry %s executed (about to succeed).", currentTimeMillis(), retriable.key()));

            SuccessCallback callback = invocation.getArgumentAt(0, SuccessCallback.class);
            callback.onSuccess((Retriable) invocation.getMock());
            return null;

        }).when(retriable).retry(any(SuccessCallback.class), any(ErrorCallback.class));
    }

    public static void failedNoRetry(Retriable retriable, Throwable cause) {
        doAnswer(invocation -> {

            out.println(format("[%d] Retry %s executed (about to fail no retry).", currentTimeMillis(), retriable.key()));

            ErrorCallback callback = invocation.getArgumentAt(1, ErrorCallback.class);
            callback.onError(new RetryException(cause, retriable, false));

            return null;

        }).when(retriable).retry(any(SuccessCallback.class), any(ErrorCallback.class));
    }

    /**
     * The invocationCallback is intended to setup the next mock condition.
     */
    public static void failedCanRetry(
            Retriable retriable, Throwable cause, long retryAtTimeMillis, Runnable invocationCallback) {

        doAnswer(invocation -> {

            out.println(format("[%d] Retry %s executed (about to fail could retry).", currentTimeMillis(), retriable.key()));

            when(retriable.retryAtTimeMillis()).thenReturn(retryAtTimeMillis);
            invocationCallback.run();

            ErrorCallback callback = invocation.getArgumentAt(1, ErrorCallback.class);
            callback.onError(new RetryException(cause, retriable, true));

            return null;

        }).when(retriable).retry(any(SuccessCallback.class), any(ErrorCallback.class));
    }

}
