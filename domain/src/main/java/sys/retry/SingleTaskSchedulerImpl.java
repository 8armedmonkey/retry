package sys.retry;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.lang.System.currentTimeMillis;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class SingleTaskSchedulerImpl implements SingleTaskScheduler {

    private final ScheduledExecutorService executorService;
    private ScheduledFuture scheduledFuture;

    @SuppressWarnings("WeakerAccess")
    public SingleTaskSchedulerImpl() {
        executorService = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public synchronized void schedule(Runnable runnable, long timeMillis) {
        cancel();
        scheduledFuture = executorService.schedule(
                runnable, timeMillis - currentTimeMillis(), MILLISECONDS);
    }

    @Override
    public synchronized void cancel() {
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
            scheduledFuture = null;
        }
    }

}
