package sys.retry;

import static java.lang.System.currentTimeMillis;

public abstract class BaseBackOffStrategy implements RetryStrategy {

    @SuppressWarnings("WeakerAccess")
    public static final int INDEFINITE_MAX_RETRIES = -1;

    private final int maxRetries;
    private final BackOffInterval backOffInterval;
    private int retries;

    private Long retryAtTimeMillis;

    /**
     * If maxRetries is set to {@link #INDEFINITE_MAX_RETRIES} (or any negative number),
     * it will be retried indefinitely or until a fatal error occurs.
     */
    public BaseBackOffStrategy(int maxRetries, BackOffInterval backOffInterval) {
        this.maxRetries = maxRetries;
        this.backOffInterval = backOffInterval;
        this.retries = 0;
    }

    @Override
    public synchronized long retryAtTimeMillis() {
        if (retryAtTimeMillis == null) {
            updateRetryAtTimeMillis();
        }
        return retryAtTimeMillis;
    }

    @Override
    public synchronized boolean onRetryFailed(Throwable failedRetryCause) {
        boolean shouldRetry = (maxRetries < 0 || ++retries < maxRetries) && !isFatalError(failedRetryCause);

        if (shouldRetry) {
            backOffInterval.backOff();
            updateRetryAtTimeMillis();
        } else {
            backOffInterval.reset();
        }

        return shouldRetry;
    }

    private void updateRetryAtTimeMillis() {
        retryAtTimeMillis = currentTimeMillis() + backOffInterval.millis();
    }

}
