package sys.retry;

import org.junit.Before;
import org.junit.Test;
import sys.test.provider.RetriableProvider;

import static org.junit.Assert.assertEquals;

public class TestRetryQueueAndComparator {

    private RetryQueue<Retriable> retryQueue;

    @Before
    public void setUp() {
        retryQueue = new RetryQueueImpl<>(new RetryAtTimeMillisComparator());
    }

    @Test
    public void put_orderedByTimeMillis() {
        Retriable r1 = RetriableProvider.simple("KEY_1", 4000L);
        Retriable r2 = RetriableProvider.simple("KEY_2", 2000L);
        Retriable r3 = RetriableProvider.simple("KEY_3", 1000L);
        Retriable r4 = RetriableProvider.simple("KEY_4", 3000L);

        retryQueue.put(r1);
        retryQueue.put(r2);
        retryQueue.put(r3);
        retryQueue.put(r4);

        assertEquals(r3, retryQueue.take());
        assertEquals(r2, retryQueue.take());
        assertEquals(r4, retryQueue.take());
        assertEquals(r1, retryQueue.take());
    }

}
