package sys.retry;

public class BackOffIntervalImpl implements BackOffInterval {

    private final BackOff backOff;
    private final double multiplierMillis;

    @SuppressWarnings("WeakerAccess")
    public BackOffIntervalImpl(
            BackOff backOff,
            double multiplierMillis) {

        this.backOff = backOff;
        this.multiplierMillis = multiplierMillis;
    }

    @Override
    public void backOff() {
        backOff.increment();
    }

    @Override
    public void reset() {
        backOff.reset();
    }

    @Override
    public long millis() {
        return new Double(backOff.current() * multiplierMillis).longValue();
    }

}
