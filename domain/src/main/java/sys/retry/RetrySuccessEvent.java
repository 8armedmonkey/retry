package sys.retry;

import sys.event.Event;

public class RetrySuccessEvent implements Event {

    private final Retriable retriable;

    public RetrySuccessEvent(Retriable retriable) {
        this.retriable = retriable;
    }

    public Retriable getRetriable() {
        return retriable;
    }

}
