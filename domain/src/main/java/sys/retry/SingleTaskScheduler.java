package sys.retry;

public interface SingleTaskScheduler {

    void schedule(Runnable runnable, long timeMillis);

    void cancel();

}
