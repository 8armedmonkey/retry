package sys.retry;

import org.junit.Before;
import org.junit.Test;
import sys.test.util.MockRunnable;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static sys.test.util.Helpers.sleep;

public class TestSingleTaskScheduler {

    private SingleTaskScheduler scheduler;

    @Before
    public void setUp() {
        scheduler = new SingleTaskSchedulerImpl();
    }

    @Test
    public void schedule_taskExecutedAtTheScheduledTime() {
        MockRunnable runnable = spy(new MockRunnable());
        long currentTimeMillis = currentTimeMillis();
        long expectedRunAtMillis = currentTimeMillis + 2000L;

        scheduler.schedule(runnable, expectedRunAtMillis);

        sleep(2100L);

        verify(runnable, times(1)).run();
        assertEquals(expectedRunAtMillis, runnable.getRunAtMillis(), 10);
    }

    @Test
    public void schedule_alreadyHasTaskScheduled_timeIsUpdated() {
        MockRunnable runnable = spy(new MockRunnable());
        long currentTimeMillis = currentTimeMillis();
        long expectedRunAtMillis = currentTimeMillis + 1000L;

        scheduler.schedule(runnable, currentTimeMillis + 3000L);
        scheduler.schedule(runnable, expectedRunAtMillis);

        sleep(3100L);

        verify(runnable, times(1)).run();
        assertEquals(expectedRunAtMillis, runnable.getRunAtMillis(), 10);
    }

    @Test
    public void schedule_timeInThePast_taskExecutedImmediately() {
        MockRunnable runnable = spy(new MockRunnable());
        long currentTimeMillis = currentTimeMillis();

        scheduler.schedule(runnable, currentTimeMillis - 3000L);

        sleep(100L);

        verify(runnable, times(1)).run();
        assertEquals(currentTimeMillis, runnable.getRunAtMillis(), 10);
    }

}
