package sys.retry;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestConstantInterval {

    @Test
    public void millis_alwaysReturnsTheSameInterval() {
        ConstantInterval interval = new ConstantInterval(1000L);
        assertEquals(1000L, interval.millis());
    }

}
