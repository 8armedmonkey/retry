package sys.retry;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;
import sys.test.provider.EventBroadcasterProvider;
import sys.test.provider.RetriableProvider;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static sys.test.util.Helpers.sleep;

public class TestRetrySystemQueueProcessing {

    private RetryQueue<Retriable> queue;
    private SingleTaskScheduler scheduler;
    private EventBroadcaster eventBroadcaster;
    private RetrySystem<Retriable> retrySystem;

    @Before
    public void setUp() {
        queue = spy(new RetryQueueImpl<>(new RetryAtTimeMillisComparator()));
        scheduler = spy(new SingleTaskSchedulerImpl());
        eventBroadcaster = EventBroadcasterProvider.simple();
        retrySystem = new RetrySystemImpl<>(queue, scheduler, eventBroadcaster);
    }

    @Test
    public void put_retriable_retryIsScheduled() {
        long currentTimeMillis = currentTimeMillis();

        Retriable r1 = RetriableProvider.simple("KEY_1", currentTimeMillis + 2000L);
        Retriable r2 = RetriableProvider.simple("KEY_2", currentTimeMillis + 1000L);
        Retriable r3 = RetriableProvider.simple("KEY_3", currentTimeMillis + 3000L);

        retrySystem.put(r1);
        retrySystem.put(r2);
        retrySystem.put(r3);

        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        verify(scheduler, times(2)).schedule(any(Runnable.class), captor.capture());

        assertEquals(2, captor.getAllValues().size());

        // The earliest time will be chosen. Later time will not trigger anymore scheduling.
        assertEquals(currentTimeMillis + 2000L, captor.getAllValues().get(0).longValue());
        assertEquals(currentTimeMillis + 1000L, captor.getAllValues().get(1).longValue());
    }

    @Test
    public void put_retriable_allScheduledRetriablesAreTaken() {
        long currentTimeMillis = currentTimeMillis();

        Retriable r1 = RetriableProvider.simple("KEY_1", currentTimeMillis + 2000L);
        Retriable r2 = RetriableProvider.simple("KEY_2", currentTimeMillis + 1000L);
        Retriable r3 = RetriableProvider.simple("KEY_3", currentTimeMillis + 3000L);

        retrySystem.put(r1);
        retrySystem.put(r2);
        retrySystem.put(r3);

        sleep(3100L);

        verify(queue, times(3)).take();
    }

    @Test
    public void onRetriableTaken_nextRetryIsScheduled() {
        long currentTimeMillis = currentTimeMillis();

        Retriable r1 = RetriableProvider.simple("KEY_1", currentTimeMillis + 2000L);
        Retriable r2 = RetriableProvider.simple("KEY_2", currentTimeMillis + 1000L);
        Retriable r3 = RetriableProvider.simple("KEY_3", currentTimeMillis + 3000L);

        retrySystem.put(r1);
        retrySystem.put(r2);
        retrySystem.put(r3);

        sleep(1100L);

        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        verify(scheduler, times(3)).schedule(any(Runnable.class), captor.capture());

        assertEquals(3, captor.getAllValues().size());

        // During put of r1.
        assertEquals(currentTimeMillis + 2000L, captor.getAllValues().get(0).longValue());

        // During put of r2.
        assertEquals(currentTimeMillis + 1000L, captor.getAllValues().get(1).longValue());

        // The third time is after r1 is picked up.
        // It is using r2.retryAtTimeMillis() since it is the head of the queue now.
        assertEquals(currentTimeMillis + 2000L, captor.getAllValues().get(2).longValue());
    }

    @Test
    public void onRetriableTaken_queueIsEmpty_noSchedulingHappens() {
        long currentTimeMillis = currentTimeMillis();

        Retriable r = RetriableProvider.simple("KEY", currentTimeMillis + 1000L);
        retrySystem.put(r);

        sleep(1100L);

        verify(scheduler, times(1)).schedule(any(Runnable.class), eq(currentTimeMillis + 1000L));
    }

    @Test
    public void onRetriableTaken_takenRetriableIsRetried() {
        long currentTimeMillis = currentTimeMillis();

        Retriable r = RetriableProvider.simple("KEY", currentTimeMillis + 1000L);
        retrySystem.put(r);

        sleep(1100L);

        verify(r).retry(any(SuccessCallback.class), any(ErrorCallback.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void subscribe_subscribeToEventBroadcast() {
        retrySystem.subscribe(RetrySuccessEvent.class, mock(EventHandler.class));
        verify(eventBroadcaster).subscribe(any(Class.class), any(EventHandler.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void onRetrySuccess_successEventBroadcast() {
        Retriable r = RetriableProvider.simple("KEY", currentTimeMillis() + 1000L);
        retrySystem.onRetrySuccess(r);

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(eventBroadcaster).broadcast(captor.capture());

        assertEquals(RetrySuccessEvent.class, captor.getValue().getClass());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void onRetryError_errorEventBroadcast() {
        Retriable r = RetriableProvider.simple("KEY", currentTimeMillis() + 1000L);
        retrySystem.onRetryError(new RetryException(new Exception(), r, false));

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(eventBroadcaster).broadcast(captor.capture());

        assertEquals(RetryFailedEvent.class, captor.getValue().getClass());
    }

    @Test
    public void onRetryError_canRetry_retryIsScheduledAgain() {
        Retriable r = RetriableProvider.simple("KEY", currentTimeMillis() + 1000L);
        retrySystem.onRetryError(new RetryException(new Exception(), r, true));

        verify(queue).put(r);
    }

    @Test
    public void onRetryError_canNotRetry_retryIsNotScheduledAgain() {
        Retriable r = RetriableProvider.simple("KEY", currentTimeMillis() + 1000L);
        retrySystem.onRetryError(new RetryException(new Exception(), r, false));

        verify(queue, never()).put(r);
    }

}
