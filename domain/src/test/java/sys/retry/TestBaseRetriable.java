package sys.retry;

import org.junit.Before;
import org.junit.Test;
import sys.test.provider.SimpleBackOffStrategy;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;

public class TestBaseRetriable {

    private BaseRetriable retriable;

    @Before
    public void setUp() {
        retriable = mock(BaseRetriable.class, CALLS_REAL_METHODS);
    }

    @Test
    public void retryAtTimeMillis_initialState_returnInitialRetryAtTimeMillis() {
        RetryStrategy strategy = new SimpleBackOffStrategy(
                10,
                new BackOffIntervalImpl(new DeterministicExponentialBackOff(5), 1000.0),
                failedRetryCause -> false);

        retriable.use(strategy);

        assertEquals(strategy.retryAtTimeMillis(), retriable.retryAtTimeMillis());
    }

    @Test
    public void retryAtTimeMillis_afterFailedAttempt_returnNewRetryAtTimeMillis() {
        RetryStrategy strategy = new SimpleBackOffStrategy(
                10,
                new BackOffIntervalImpl(new DeterministicExponentialBackOff(5), 1000.0),
                failedRetryCause -> false);

        retriable.use(strategy);
        strategy.onRetryFailed(new Exception());

        assertEquals(strategy.retryAtTimeMillis(), retriable.retryAtTimeMillis());
    }

    @Test
    public void use_retryStrategy_newRetryStrategyIsUsed() {
        RetryStrategy strategy = mock(RetryStrategy.class);
        retriable.use(strategy);

        assertEquals(strategy, retriable.strategy);
    }

    @Test(expected = IllegalArgumentException.class)
    public void use_null_throwIllegalArgumentException() {
        retriable.use(null);
    }

}
