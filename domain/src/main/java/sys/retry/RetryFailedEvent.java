package sys.retry;

import sys.event.Event;

public class RetryFailedEvent implements Event {

    private final RetryException error;

    public RetryFailedEvent(RetryException error) {
        this.error = error;
    }

    public RetryException getError() {
        return error;
    }

}
