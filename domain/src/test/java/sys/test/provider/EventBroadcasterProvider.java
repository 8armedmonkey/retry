package sys.test.provider;

import sys.event.EventBroadcaster;

import static org.mockito.Mockito.spy;

public class EventBroadcasterProvider {

    public static EventBroadcaster simple() {
        return spy(new SimpleEventBroadcaster());
    }

}
