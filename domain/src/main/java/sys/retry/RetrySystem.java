package sys.retry;

import sys.event.Event;
import sys.event.EventBroadcaster.Subscription;
import sys.event.EventHandler;

public interface RetrySystem<R extends Retriable> {

    void put(R retriable);

    void remove(String key);

    boolean contains(String key);

    boolean isQueueEmpty();

    void onRetrySuccess(R retriable);

    void onRetryError(RetryException error);

    <E extends Event> Subscription subscribe(Class<E> eventClass, EventHandler<E> eventHandler);

}
