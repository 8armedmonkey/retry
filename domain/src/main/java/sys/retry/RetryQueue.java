package sys.retry;

public interface RetryQueue<R extends Retriable> {

    void put(R retriable);

    R take();

    R remove(String key);

    boolean contains(String key);

    boolean isEmpty();

    void clear();

    long earliestRetryAtTimeMillis();

}
