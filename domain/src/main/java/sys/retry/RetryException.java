package sys.retry;

public class RetryException extends Exception {

    private final Retriable retriable;
    private final boolean shouldRetryAgain;

    public RetryException(Throwable cause, Retriable retriable, boolean shouldRetryAgain) {
        super(cause);

        this.retriable = retriable;
        this.shouldRetryAgain = shouldRetryAgain;
    }

    public Retriable getRetriable() {
        return retriable;
    }

    public boolean shouldRetryAgain() {
        return shouldRetryAgain;
    }

}
