package sys.retry;

import org.junit.Before;
import org.junit.Test;
import sys.test.provider.RetriableProvider;

import static org.junit.Assert.assertEquals;

public class TestRetryAtTimeMillisComparator {

    private RetriableComparator<Retriable> comparator;

    @Before
    public void setUp() {
        comparator = new RetryAtTimeMillisComparator();
    }

    @Test
    public void compare_negativeOne() {
        Retriable r1 = RetriableProvider.simple("KEY_1", 1000L);
        Retriable r2 = RetriableProvider.simple("KEY_2", 2000L);

        assertEquals(-1, comparator.compare(r1, r2));
    }

    @Test
    public void compare_zero() {
        Retriable r1 = RetriableProvider.simple("KEY_1", 1000L);
        Retriable r2 = RetriableProvider.simple("KEY_2", 1000L);

        assertEquals(0, comparator.compare(r1, r2));
    }

    @Test
    public void compare_positiveOne() {
        Retriable r1 = RetriableProvider.simple("KEY_1", 2000L);
        Retriable r2 = RetriableProvider.simple("KEY_2", 1000L);

        assertEquals(1, comparator.compare(r1, r2));
    }

}
