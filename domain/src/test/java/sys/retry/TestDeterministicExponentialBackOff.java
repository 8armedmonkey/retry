package sys.retry;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDeterministicExponentialBackOff {

    private int max;
    private BackOff backOff;

    @Before
    public void setUp() {
        max = 10;
        backOff = new DeterministicExponentialBackOff(max);
    }

    @Test
    public void current_currentCounterIsReturned() {
        backOff.increment();
        backOff.increment();
        backOff.increment();

        // 2 ^ 3 - 1
        assertEquals(7, backOff.current());
    }

    @Test
    public void increment_hasNotReachedMaxCounter_backOffTimeIncreased() {
        backOff.increment();
        backOff.increment();
        backOff.increment();

        // 2 ^ 4 - 1
        assertEquals(15, backOff.increment());
    }

    @Test
    public void increment_hasReachedMaxCounter_backOffTimeRemains() {
        for (int i = 0; i < max; i++) {
            backOff.increment();
        }

        // 2 ^ 10 - 1
        assertEquals(1023, backOff.increment());
    }

    @Test
    public void reset_counterIsReset() {
        backOff.increment();
        backOff.increment();
        backOff.increment();

        assertEquals(0, backOff.reset());
        assertEquals(0, backOff.current());
    }

}
