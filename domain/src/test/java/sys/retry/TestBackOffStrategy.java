package sys.retry;

import org.junit.Test;
import sys.test.provider.SimpleBackOffStrategy;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static sys.retry.BaseBackOffStrategy.INDEFINITE_MAX_RETRIES;

public class TestBackOffStrategy {

    @Test
    public void maxRetriesNegative_nonFatalError_shouldRetryReturnTrue() {
        RetryStrategy strategy = new SimpleBackOffStrategy(
                INDEFINITE_MAX_RETRIES,
                new BackOffIntervalImpl(new DeterministicExponentialBackOff(5), 1000.0),
                failedRetryCause -> false);

        for (int i = 0; i < 100; i++) {
            assertTrue(strategy.onRetryFailed(new Exception()));
        }
    }

    @Test
    public void maxRetriesNegative_fatalError_shouldRetryReturnFalse() {
        RetryStrategy strategy = new SimpleBackOffStrategy(
                INDEFINITE_MAX_RETRIES,
                new BackOffIntervalImpl(new DeterministicExponentialBackOff(5), 1000.0),
                failedRetryCause -> true);

        assertFalse(strategy.onRetryFailed(new Exception()));
    }

    @Test
    public void maxRetriesHasNotBeenReached_nonFatalError_shouldRetryReturnTrue() {
        RetryStrategy strategy = new SimpleBackOffStrategy(
                10,
                new BackOffIntervalImpl(new DeterministicExponentialBackOff(5), 1000.0),
                failedRetryCause -> false);

        assertTrue(strategy.onRetryFailed(new Exception()));
    }

    @Test
    public void maxRetriesHasNotBeenReached_fatalError_shouldRetryReturnFalse() {
        RetryStrategy strategy = new SimpleBackOffStrategy(
                10,
                new BackOffIntervalImpl(new DeterministicExponentialBackOff(5), 1000.0),
                failedRetryCause -> true);

        assertFalse(strategy.onRetryFailed(new Exception()));
    }

    @Test
    public void maxRetriesHasBeenReached_nonFatalError_shouldRetryReturnFalse() {
        RetryStrategy strategy = new SimpleBackOffStrategy(
                2,
                new BackOffIntervalImpl(new DeterministicExponentialBackOff(5), 1000.0),
                failedRetryCause -> false);

        assertTrue(strategy.onRetryFailed(new Exception()));
        assertFalse(strategy.onRetryFailed(new Exception()));
    }

    @Test
    public void maxRetriesHasBeenReached_fatalError_shouldRetryReturnFalse() {
        RetryStrategy strategy = new SimpleBackOffStrategy(
                2,
                new BackOffIntervalImpl(new DeterministicExponentialBackOff(5), 1000.0),
                failedRetryCause -> true);

        assertFalse(strategy.onRetryFailed(new Exception()));
        assertFalse(strategy.onRetryFailed(new Exception()));
    }

}
