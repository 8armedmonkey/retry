package sys.retry;

public interface RetryStrategy {

    long retryAtTimeMillis();

    boolean onRetryFailed(Throwable failedRetryCause);

    boolean isFatalError(Throwable failedRetryCause);

}
