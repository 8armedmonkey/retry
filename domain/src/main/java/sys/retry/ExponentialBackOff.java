package sys.retry;

import java.util.Random;

/**
 * The values are non-deterministic, it involves randomization from an exponential range to generate the value.
 * Use {@link DeterministicExponentialBackOff} for deterministic values.
 */
public class ExponentialBackOff implements BackOff {

    private final Random random;
    private final int[] slots;
    private final int max;
    private int index;
    private int current;

    public ExponentialBackOff(int max) {
        this.random = new Random();
        this.slots = new int[max + 1];
        this.max = max;
        this.index = 0;

        fillSlots(slots);

        this.current = randomBackOffValue(random, slots, index);
    }

    private static void fillSlots(int[] slots) {
        for (int i = 0; i < slots.length; i++) {
            slots[i] = exponent(i);
        }
    }

    private static int exponent(int e) {
        return new Double(Math.pow(2, e) - 1).intValue();
    }

    private static int randomBackOffValue(Random random, int[] slots, int maxIndex) {
        return slots[random.nextInt(maxIndex + 1)];
    }

    @Override
    public int increment() {
        return current = randomBackOffValue(random, slots, index = Math.min(max, index + 1));
    }

    @Override
    public int current() {
        return current;
    }

    @Override
    public int reset() {
        return current = randomBackOffValue(random, slots, index = 0);
    }

}
