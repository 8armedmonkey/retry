package sys.retry;

import java.util.Comparator;

public interface RetriableComparator<R extends Retriable> extends Comparator<R> {
}
