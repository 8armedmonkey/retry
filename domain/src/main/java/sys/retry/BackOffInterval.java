package sys.retry;

public interface BackOffInterval extends Interval {

    void backOff();

    void reset();

}
