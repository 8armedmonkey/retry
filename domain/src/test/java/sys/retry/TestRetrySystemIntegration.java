package sys.retry;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import sys.event.EventBroadcaster.Subscription;
import sys.event.EventHandler;
import sys.test.provider.EventBroadcasterProvider;

import java.util.concurrent.ExecutionException;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static sys.test.provider.RetriableProvider.*;
import static sys.test.util.Helpers.sleep;

public class TestRetrySystemIntegration {

    private RetrySystem<Retriable> retrySystem;
    private EventHandler<RetrySuccessEvent> successHandler;
    private EventHandler<RetryFailedEvent> failedHandler;
    private Subscription successSubscription;
    private Subscription failedSubscription;
    private long currentTimeMillis;

    @Before
    public void setUp() {
        retrySystem = new RetrySystemImpl<>(
                new RetryQueueImpl<>(new RetryAtTimeMillisComparator()),
                new SingleTaskSchedulerImpl(),
                EventBroadcasterProvider.simple());

        successHandler = mock(RetrySuccessEventHandler.class);
        failedHandler = mock(RetryFailedEventHandler.class);

        successSubscription = retrySystem.subscribe(RetrySuccessEvent.class, successHandler);
        failedSubscription = retrySystem.subscribe(RetryFailedEvent.class, failedHandler);

        currentTimeMillis = currentTimeMillis();
    }

    @Test
    public void allSuccess() {
        Retriable r1 = simple("KEY_1", currentTimeMillis + 500L);
        Retriable r2 = simple("KEY_2", currentTimeMillis + 1500L);
        Retriable r3 = simple("KEY_3", currentTimeMillis + 1000L);

        success(r1);
        success(r2);
        success(r3);

        retrySystem.put(r1);
        retrySystem.put(r2);
        retrySystem.put(r3);

        sleep(1600);

        ArgumentCaptor<RetrySuccessEvent> captor = ArgumentCaptor.forClass(RetrySuccessEvent.class);
        verify(successHandler, times(3)).handle(captor.capture());
        verify(failedHandler, never()).handle(any(RetryFailedEvent.class));

        assertEquals("KEY_1", captor.getAllValues().get(0).getRetriable().key());
        assertEquals("KEY_3", captor.getAllValues().get(1).getRetriable().key());
        assertEquals("KEY_2", captor.getAllValues().get(2).getRetriable().key());
    }

    @Test
    public void someFailedNoRetry() {
        Retriable r1 = simple("KEY_1", currentTimeMillis + 500L);
        Retriable r2 = simple("KEY_2", currentTimeMillis + 1500L);
        Retriable r3 = simple("KEY_3", currentTimeMillis + 1000L);

        failedNoRetry(r1, new Exception());
        failedNoRetry(r2, new Exception());
        success(r3);

        retrySystem.put(r1);
        retrySystem.put(r2);
        retrySystem.put(r3);

        sleep(1600);

        ArgumentCaptor<RetrySuccessEvent> successCaptor = ArgumentCaptor.forClass(RetrySuccessEvent.class);
        verify(successHandler, times(1)).handle(successCaptor.capture());

        ArgumentCaptor<RetryFailedEvent> failedCaptor = ArgumentCaptor.forClass(RetryFailedEvent.class);
        verify(failedHandler, times(2)).handle(failedCaptor.capture());

        assertEquals("KEY_3", successCaptor.getAllValues().get(0).getRetriable().key());

        assertEquals("KEY_1", failedCaptor.getAllValues().get(0).getError().getRetriable().key());
        assertEquals("KEY_2", failedCaptor.getAllValues().get(1).getError().getRetriable().key());
    }

    @Test
    public void someFailedCanRetry() throws ExecutionException, InterruptedException {
        Retriable r1 = simple("KEY_1", currentTimeMillis + 500L);
        Retriable r2 = simple("KEY_2", currentTimeMillis + 1500L);
        Retriable r3 = simple("KEY_3", currentTimeMillis + 1000L);

        // Failed twice before success.
        failedCanRetry(
                r1,
                new Exception(),
                currentTimeMillis + 750L,
                () -> failedCanRetry(
                        r1,
                        new Exception(),
                        currentTimeMillis + 1250L,
                        () -> success(r1)));

        // Failed once before success.
        failedCanRetry(r2, new Exception(), currentTimeMillis + 2000L, () -> success(r2));

        // Failed once with retry before failed with no retry.
        failedCanRetry(r3, new Exception(), currentTimeMillis + 2500L, () -> failedNoRetry(r3, new Exception()));

        retrySystem.put(r1);
        retrySystem.put(r2);
        retrySystem.put(r3);

        sleep(3000L);

        ArgumentCaptor<RetrySuccessEvent> successCaptor = ArgumentCaptor.forClass(RetrySuccessEvent.class);
        verify(successHandler, times(2)).handle(successCaptor.capture());

        ArgumentCaptor<RetryFailedEvent> failedCaptor = ArgumentCaptor.forClass(RetryFailedEvent.class);
        verify(failedHandler, times(5)).handle(failedCaptor.capture());

        assertEquals("KEY_1", successCaptor.getAllValues().get(0).getRetriable().key());
        assertEquals("KEY_2", successCaptor.getAllValues().get(1).getRetriable().key());

        assertEquals("KEY_1", failedCaptor.getAllValues().get(0).getError().getRetriable().key());
        assertEquals("KEY_1", failedCaptor.getAllValues().get(1).getError().getRetriable().key());
        assertEquals("KEY_3", failedCaptor.getAllValues().get(2).getError().getRetriable().key());
        assertEquals("KEY_2", failedCaptor.getAllValues().get(3).getError().getRetriable().key());
        assertEquals("KEY_3", failedCaptor.getAllValues().get(4).getError().getRetriable().key());
    }

    @After
    public void tearDown() {
        // For the sake of removing the warning.
        successSubscription.unsubscribe();
        failedSubscription.unsubscribe();
    }

}
