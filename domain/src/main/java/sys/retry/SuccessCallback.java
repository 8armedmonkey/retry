package sys.retry;

public interface SuccessCallback {

    void onSuccess(Retriable retriable);

}
