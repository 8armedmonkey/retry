package sys.retry;

/**
 * Similar to {@link ExponentialBackOff} but the values are deterministic (without randomization on the range).
 */
public class DeterministicExponentialBackOff implements BackOff {

    private final int max;
    private int index;

    public DeterministicExponentialBackOff(int max) {
        this.max = max;
        this.index = 0;
    }

    private static int exponent(int index) {
        return new Double(Math.pow(2, index) - 1).intValue();
    }

    @Override
    public int increment() {
        return exponent(index = Math.min(max, index + 1));
    }

    @Override
    public int current() {
        return exponent(index);
    }

    @Override
    public int reset() {
        return exponent(index = 0);
    }

}
