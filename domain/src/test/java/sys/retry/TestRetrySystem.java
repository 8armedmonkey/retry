package sys.retry;

import org.junit.Before;
import org.junit.Test;
import sys.test.provider.EventBroadcasterProvider;
import sys.test.provider.RetriableProvider;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestRetrySystem {

    private RetrySystem<Retriable> retrySystem;

    @Before
    public void setUp() {
        retrySystem = new RetrySystemImpl<>(
                new RetryQueueImpl<>(new RetryAtTimeMillisComparator()),
                new SingleTaskSchedulerImpl(),
                EventBroadcasterProvider.simple());
    }

    @Test
    public void put_retriable_retriableIsAddedToQueue() {
        Retriable retriable = RetriableProvider.simple("KEY", currentTimeMillis() + 1000L);
        retrySystem.put(retriable);

        assertTrue(retrySystem.contains("KEY"));
    }

    @Test
    public void remove_keyExists_retriableIsRemovedFromQueue() {
        Retriable retriable = RetriableProvider.simple("KEY", currentTimeMillis() + 1000L);
        retrySystem.put(retriable);
        retrySystem.remove("KEY");

        assertFalse(retrySystem.contains("KEY"));
    }

    @Test
    public void remove_keyNotExist_nothingHappen() {
        Retriable retriable = RetriableProvider.simple("KEY_1", currentTimeMillis() + 1000L);
        retrySystem.put(retriable);
        retrySystem.remove("KEY_2");

        assertTrue(retrySystem.contains("KEY_1"));
    }

    @Test
    public void isQueueEmpty_nothingIsAdded_returnTrue() {
        assertTrue(retrySystem.isQueueEmpty());
    }

    @Test
    public void isQueueEmpty_itemIsAdded_returnFalse() {
        Retriable retriable = RetriableProvider.simple("KEY_1", currentTimeMillis() + 1000L);
        retrySystem.put(retriable);

        assertFalse(retrySystem.isQueueEmpty());
    }

}
