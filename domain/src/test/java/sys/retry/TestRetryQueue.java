package sys.retry;

import org.junit.Before;
import org.junit.Test;
import sys.test.provider.RetriableProvider;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class TestRetryQueue {

    private RetryQueue<Retriable> retryQueue;

    @Before
    public void setUp() {
        retryQueue = new RetryQueueImpl<>(new RetryAtTimeMillisComparator());
    }

    @Test
    public void put_itemPut() {
        retryQueue.put(RetriableProvider.simple("KEY", 1000L));
        assertTrue(retryQueue.contains("KEY"));
    }

    @Test
    public void take_nonEmpty_itemTaken() {
        retryQueue.put(RetriableProvider.simple("KEY", 1000L));
        assertEquals("KEY", retryQueue.take().key());
    }

    @Test(expected = EmptyQueueException.class)
    public void take_empty_throwEmptyQueueException() {
        retryQueue.take();
    }

    @Test
    public void remove_itemExist_returnItem() {
        retryQueue.put(RetriableProvider.simple("KEY", 1000L));
        assertNotNull(retryQueue.remove("KEY"));
    }

    @Test
    public void remove_itemNoExist_returnNull() {
        retryQueue.put(RetriableProvider.simple("KEY_1", 1000L));
        assertNull(retryQueue.remove("KEY_2"));
    }

    @Test
    public void isEmpty_nonEmpty_returnFalse() {
        retryQueue.put(RetriableProvider.simple("KEY", 1000L));
        assertFalse(retryQueue.isEmpty());
    }

    @Test
    public void isEmpty_empty_returnTrue() {
        assertTrue(retryQueue.isEmpty());
    }

    @Test
    public void clear_queueCleared() {
        retryQueue.put(RetriableProvider.simple("KEY_1", 1000L));
        retryQueue.put(RetriableProvider.simple("KEY_2", 1000L));
        retryQueue.clear();

        assertTrue(retryQueue.isEmpty());
        assertFalse(retryQueue.contains("KEY_1"));
        assertFalse(retryQueue.contains("KEY_2"));
    }

    @Test
    public void earliestRetryAtTimeMillis_nonEmptyQueue_returnQueueHeadRetryAtTimeMillis() {
        Retriable r1 = RetriableProvider.simple("KEY_1", 1000L);
        Retriable r2 = RetriableProvider.simple("KEY_2", 1000L);

        when(r1.retryAtTimeMillis()).thenReturn(1000L);
        when(r2.retryAtTimeMillis()).thenReturn(500L);

        retryQueue.put(r1);
        retryQueue.put(r2);

        assertEquals(500L, retryQueue.earliestRetryAtTimeMillis());
    }

    @Test(expected = EmptyQueueException.class)
    public void earliestRetryAtTimeMillis_emptyQueue_throwEmptyQueueException() {
        retryQueue.earliestRetryAtTimeMillis();
    }

}
