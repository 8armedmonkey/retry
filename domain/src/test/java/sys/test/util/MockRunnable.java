package sys.test.util;

import static java.lang.System.currentTimeMillis;

public class MockRunnable implements Runnable {

    private long runAtMillis;

    @Override
    public void run() {
        runAtMillis = currentTimeMillis();
    }

    public long getRunAtMillis() {
        return runAtMillis;
    }

}
