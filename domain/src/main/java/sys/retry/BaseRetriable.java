package sys.retry;

public abstract class BaseRetriable implements Retriable {

    RetryStrategy strategy;

    public BaseRetriable(RetryStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public long retryAtTimeMillis() {
        return strategy.retryAtTimeMillis();
    }

    @Override
    public void use(RetryStrategy strategy) {
        if (strategy != null) {
            this.strategy = strategy;
        } else {
            throw new IllegalArgumentException("Strategy should not be null.");
        }
    }

    @Override
    public void retry(SuccessCallback onSuccess, ErrorCallback onError) {
        try {
            retryInternal();
            onSuccess.onSuccess(this);

        } catch (Exception e) {
            onError.onError(new RetryException(e, this, strategy.onRetryFailed(e)));
        }
    }

    @SuppressWarnings("WeakerAccess")
    protected abstract void retryInternal();

}
